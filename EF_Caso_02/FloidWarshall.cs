﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_02
{
    class FloydWarshall
    {
        public const int INF = 99999;
        static string[] lugares = { "", "Casa", "Trabajo", "Univ", "Tios", "Abuelos", "Amigo 1", "Amigo 2" };
        private static void Imprimir(int[,] distancia, int TotalVertices)
        {
            Console.WriteLine("\nDistancia mínima entre cada par de vértices:\n--------------------------------------------");
            for (int k = 0; k < lugares.Length; k++)
            {
                Console.Write(lugares[k].PadLeft(9));
            }
            Console.WriteLine("");
            for (int i = 0; i < TotalVertices; ++i)
            {
                Console.Write(lugares[i + 1].PadLeft(9));
                for (int j = 0; j < TotalVertices; ++j)
                {
                    if (distancia[i, j] == INF)
                        Console.Write("INF".PadLeft(9));
                    else
                        Console.Write(distancia[i, j].ToString().PadLeft(9));
                }
                Console.WriteLine();
            }
        }

        public static void FloydWarshallAlgo(int[,] grafo, int TotalVertices)
        {
            int[,] distancia = new int[TotalVertices, TotalVertices];
            for (int i = 0; i < TotalVertices; ++i)
                for (int j = 0; j < TotalVertices; ++j)
                    distancia[i, j] = grafo[i, j];

            for (int k = 0; k < TotalVertices; ++k)
            {
                for (int i = 0; i < TotalVertices; ++i)
                {
                    for (int j = 0; j < TotalVertices; ++j)
                    {
                        if (distancia[i, k] + distancia[k, j] < distancia[i, j])
                            distancia[i, j] = distancia[i, k] + distancia[k, j];
                    }
                }
            }

            Imprimir(distancia, TotalVertices);
        }
    }
}
