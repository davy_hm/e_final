﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_02
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] modo1 =
            {
                { 0,  5,  6, 16, 17,  0,  0},
                { 7,  0,  9,  6, 15,  8,  0},
                { 0,  0,  0,  0,  0,  0,  0},
                { 0,  0,  0,  0, 10,  0,  7},
                { 6,  5, 25,  0,  0,  1,  0},
                { 0, 10,  7,  0,  0,  0,  4},
                { 0,  0, 12, 14,  0,  0,  0},
            };
            Dijkstra.DijkstraAlgo(modo1, 0,7 );

            const int INF = 99999;
            int[,] modo2 =
            {
                {   0,   5,   6,  16,  17, INF, INF},
                {   7,   0,   9,   6,  15,   8, INF},
                { INF, INF,   0, INF, INF, INF, INF},
                { INF, INF, INF,   0,  10, INF,   7},
                {   6,   5,  25, INF,   0,   1, INF},
                { INF,  10,   7, INF, INF,   0,   4},
                { INF, INF,  12,  14, INF, INF,   0},
            };

            FloydWarshall.FloydWarshallAlgo(modo2,7);

            int[,] modo3 =
            {
                { 0,  5,  6, 16,  6,  0,  0},
                { 5,  0,  9,  6,  5,  8,  0},
                { 6,  9,  0,  0, 25,  7, 12},
                {16,  6,  0,  0, 10,  0,  7},
                { 6,  5, 25, 10,  0,  1,  0},
                { 0,  8,  7,  0,  1,  0,  4},
                { 0,  0, 12,  7,  0,  4,  0},
            };
            
            Prim.PrimAlgo(modo3, 7);

            Console.WriteLine("\nOBS: En las aristas con doble valor de esfuerzo (ida y vuelta), se consideró a la de menor valor");
            Console.Read();
        }
    }
}
