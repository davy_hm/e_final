﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_01
{
    class ArbolB
    {
        public Nodo raiz;

        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InsertarNodo(int id, int prioridad, string pedido, string cliente, int costo)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo { id = id, prioridad = prioridad, pedido = pedido, cliente = cliente, costo = costo };
            if (raiz != null)
            {
                puntero = raiz;
                Console.WriteLine("Agregando nuevo  pedido de: {1} // Cliente: {0} ...", cliente, pedido);
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Agregando primer pedido de: {1} // Cliente: {0} ...", nodo.cliente,nodo.pedido);
                raiz = nodo;
            }
        }
        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("Pedido: "+raiz.pedido.PadRight(20) +"// Cliente: "+ raiz.cliente);
                Inorden(raiz.derecho);
            }
        }
        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("Pedido: " + raiz.pedido.PadRight(20) + "// Cliente: " + raiz.cliente);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }
        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("Pedido: " + raiz.pedido.PadRight(20) + "// Cliente: " + raiz.cliente);
            }
        }
       
    }
}
