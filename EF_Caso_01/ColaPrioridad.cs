﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_01
{
    class ColaPrioridad
    {
        public class Nodo
        {
            public string cliente, pedido;
            public int prioridad, costo, id;
            public Nodo siguiente;
        }
        public static Nodo Nuevo(int id, int prioridad, string pedido, string cliente, int costo)
        {
            Nodo temp = new Nodo() { id = id, prioridad = prioridad, pedido = pedido, cliente = cliente, costo = costo, siguiente = null };
            return temp;
        }
        public Nodo pop(Nodo cabeza)
        {
            if (cabeza == null)
            {
                Console.WriteLine("No hay mas pacientes en cola");
                return null;
            }
            Console.WriteLine("\nEl pedido: " + cabeza.pedido + " del cliente " + cabeza.cliente + " fue atendido...");
            cabeza = cabeza.siguiente;
            return cabeza;
        }
        public  static Nodo push(Nodo cabeza, int id, int prioridad, string pedido, string cliente,  int costo)
        {
            Nodo puntero = cabeza;
            Nodo temp = Nuevo(id, prioridad, pedido, cliente, costo);
            if (cabeza.prioridad > prioridad)
            {
                Console.WriteLine("\nIngresando pedido de: " + temp.pedido + " // Cliente: " + temp.cliente + ", ALTA PRIORIDAD");
                temp.siguiente = cabeza;
                cabeza = temp;
            }
            else
            {
                while (puntero.siguiente != null && puntero.siguiente.prioridad < prioridad)
                {
                    puntero = puntero.siguiente;
                }
                temp.siguiente = puntero.siguiente;
                puntero.siguiente = temp;
                Console.WriteLine("Ingresando pedido en cola de: " + temp.pedido + " // Cliente: " + temp.cliente  + " ...");
            }
            return cabeza;
        }
        public static void mostrar(Nodo cabeza)
        {
            Nodo temp = cabeza;
            if (temp == null)
            {
                Console.WriteLine("Cola Vacía");
            }
            Console.WriteLine("\nLista de pedidos en orden de llegada:\n-----------------------------------");
            while (temp != null)
            {
                Console.WriteLine("ID: " + temp.id + "\tCliente: " + temp.cliente.PadRight(20) + " Pedido: " + temp.pedido+" Costo: "+temp.costo);
                temp = temp.siguiente;
            }
        }
    }
}

