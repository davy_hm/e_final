﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Consultora: Yuki Consulting\n");

            Console.WriteLine("Ingreso de pedidos en estructura por orden de llegada:\n------------------------------------------------------");
            ColaPrioridad.Nodo ColaP;
            ColaP = ColaPrioridad.Nuevo(1210, 1, "Pollo + Papas ", "Hugo Ramirez", 25);
            Console.WriteLine("Ingresando  primer pedido de: " + ColaP.pedido + "// Cliente: " + ColaP.cliente + " ...");
            ColaP = ColaPrioridad.push(ColaP, 1211, 2, "1 Arroz Chaufa", "Melissa Paredes", 18);
            ColaP = ColaPrioridad.push(ColaP, 2210, 3, "Pescado frito", "Gato Cuba", 21);
            ColaP = ColaPrioridad.push(ColaP, 2314, 4, "Rolls de Canela", "Rasputin Alegría", 35);
            ColaP = ColaPrioridad.push(ColaP, 2510, 5, "Bistec a lo pobre", "Michael Jackson", 45);

            Console.WriteLine("\nIngreso de pedidos en estructura de arbol binario:\n--------------------------------------------------");
            ArbolB pedidos = new ArbolB(); 
            pedidos.InsertarNodo(2210, 3, "Pescado frito", "Gato Cuba", 21);
            pedidos.InsertarNodo(2314, 4, "Rolls de Canela", "Rasputin Alegría", 35);
            pedidos.InsertarNodo(2510, 5, "Bistec a lo pobre", "Michael Jackson", 45);
            pedidos.InsertarNodo(1210, 1, "Pollo + papas", "Hugo Ramirez", 25);
            pedidos.InsertarNodo(1211, 2, "1 Arroz Chaufa", "Melissa Paredes", 18);

            ColaPrioridad.mostrar(ColaP);

            Console.WriteLine("\nLista de pacientes INORDER:\n--------------------------");
            pedidos.Inorden(pedidos.GetRaiz());

            Console.WriteLine("\nLista de pacientes PREORDER:\n---------------------------");
            pedidos.Preorden(pedidos.GetRaiz());

            Console.WriteLine("\nLista de pacientes POSTORDER:\n----------------------------");
            pedidos.Postorden(pedidos.GetRaiz());

            ColaP = ColaPrioridad.push(ColaP, 3000, 0, "Parrila Familiar", "Homero Simpson", 200);
            pedidos.InsertarNodo(3000, 0, "Parrilla Familiar", "Homero Simpson", 200);

            ColaPrioridad.mostrar(ColaP);

            Console.WriteLine("\nLista de pacientes INORDER:\n--------------------------");
            pedidos.Inorden(pedidos.GetRaiz());

            Console.WriteLine("\nLista de pacientes PREORDER:\n---------------------------");
            pedidos.Preorden(pedidos.GetRaiz());

            Console.WriteLine("\nLista de pacientes POSTORDER:\n----------------------------");
            pedidos.Postorden(pedidos.GetRaiz());


            Console.ReadLine();
        }
    }
}
