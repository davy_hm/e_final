﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Caso_01
{
    class Nodo
    {
        public string cliente, pedido;
        public int prioridad, costo, id;
        public Nodo izquierdo;
        public Nodo derecho;
    }
}
